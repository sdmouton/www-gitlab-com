---
layout: job_page
title: "Field Marketing Manager"
---

## Responsibilities

* Swag management
* Event strategy
* Event logistics in support of the team. From helping to book space for meetups to making sure the booth is staffed, and making sure every aspect of our events are well organized.
